# DiD_approval

Difference in Difference model to assess prime path model omitted variable bias and endogeneity

## Introduction and Summary

This analysis seeks to determine the effect of a higher or lower approval amount on lease second payment default rates using a "difference in difference" linear regression model.

The motivation behind the creation of this model is the creation of the updated credit underwriting model and the potential for omitted variable bias due to the exclusion of "approval amount" from the set of variables. Theoretically, the amount of money borrowed should have a high impact on whether or not the lessee is able to make the first out of store payment in full. However, this number is computed as a function of an applicant's predicted default rate and is not available during model scoring. In addition, because approval amount is a function of the risk assigned to each applicant, the approval amount can be thought of as a composite feature of other independent variables. At the same time, omitting such a variable will undoubtedly cause some omitted variable bias and model endogeneity issues.

On August 29th 2019, the approval amounts of Metro stores that were not originally part of "Zibby" cohort of stores were changed to be higher for applicants with higher risk. Notably, a de_decision_score of 0.056 was used as the cutoff for $1,500 approval amount instead of the lower 0.01 cutoff. The approval amount cutoff for $1,100 was also changed from 0.05 to 0.079 and similarly 0.073 to 0.09 for the $1,000 approval amount.

The null hypothesis is that there was no increase in fpd due to the change in approval amount cutoffs.

The model found that there was an 0.024 increase in fpd due to increasing the approval amount; however, the difference was not found to be statistically significant at the 0.05 or 0.1 level. Therefore, this study fails to reject the null hypothesis.

## Data

MetroPCS Prime path leases where the V4 score was <= 0.084 and at least $900 approval amount. The new lease cutoffs for non-Zibby stores were changed on Aug 29th, 2019 whereas the Zibby stores stayed the same. Six weeks prior and post the cutoff change date were used as the date range of the analysis.

Applications with approval amounts that differed from the main approval cutoff amounts were excluded from the analysis. e.g. only applicants with approval amounts equivalent to [900, 1000, 1100, 1500] were included in the analysis. The final model used 13,314 observations. 


## Model

The linear model specification is as follows:
    y = beta_0 + beta_1 * post + beta_2 * treatment + beta_3 * post * treatment + beta_4 * de_decision_score

where post is whether or not the lease originated post Aug 29th and treatment is whether or not the lease originated in a store where approval amounts where raised; the dependent variable, y, is binary for 2mp60, de_decision_score is used to control for variation in individual applicant quality

Typically, logistic regression is used when the dependent variable is binary in nature; however, difference in difference model assumptions include linearity of treatment effects. Since this is an inference model rather than predictive, it won't matter much if the lower and upper bounds of the [0, 1] predictive interval is violated.

## Results

```
Coefficients:
                 Estimate Std. Error t value Pr(>|t|)    
(Intercept)      0.014854   0.013553   1.096    0.273    
treatment       -0.005196   0.013425  -0.387    0.699    
post            -0.020660   0.019018  -1.086    0.277    
de_score_points  0.976797   0.085155  11.471   <2e-16 ***
treatment:post   0.023987   0.019383   1.238    0.216    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.211 on 13309 degrees of freedom
Multiple R-squared:  0.009986,  Adjusted R-squared:  0.009688 
F-statistic: 33.56 on 4 and 13309 DF,  p-value: < 2.2e-16
```

## Files in the code folder:

*constants.py* - contains constants used by other scripts

*data_pull.sql* - sql script to pull in data from mysql

*date_split_find.py* - standalone script that attempts to find exact time of approval amount cutoff change

*DiD_model.R* - modeling code

*get_mysql.py* - python script to pull in raw data by reading in data_pull.sql

*preprocessing.py* - clean and preprocess raw data to feed into DiD_model.R file
