#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 12:39:39 2019

@author: xiaowei
"""

sql_file = 'data_pull.sql'

data_path = '../data/'
sql_data = data_path + 'raw_data.csv'


time_split = 1567119689 + 1

bucket_dict = {
    '0.073': [1000, 1100]
    , '0.05': [1100, 1500]
    , '0.084': [900, 1000]
}

preprocess_output = data_path + 'clean.csv'