select case when location_id_transaction in (6203, 6204, 6205, 6207, 6208, 6209, 6211, 6212, 6214, 6215, 6216, 6217, 6218, 6219
                , 6220, 8269, 8270, 8271, 8272, 8273, 8274, 8275, 8277, 8278, 8280, 8281, 8467, 8468, 8471, 8473, 8890, 13305
                , 13308, 13309, 13311, 13317, 13326, 13333, 16055, 17227, 18403, 18470, 18471, 20304, 22230, 31146, 31147, 31148
                , 31149, 31150, 31151, 32477, 32478, 32481, 32510, 33118, 33119, 33120, 35287, 36033, 38291, 38292, 38294, 41855
                , 41856, 42491, 42703, 43538, 43877, 46025
                , 46674, 46873, 47353, 47390, 47657, 47658, 47938, 48545, 48693, 87652, 87653, 87654, 87655, 87656, 87657, 87658, 87659
                , 87660, 89739, 91859, 92199) then 0 else 1 end as treatment, round(approval_amount/50) * 50 as approval_amount, de_score_points
, case when run_time_ms > 1567119689000 then 1 else 0 end as post
, case when first_inst_amount_60dpd - first_inst_amount_paid_60dpd > 1 then 1 else 0 end fpd
from lease_summaries ls join mongo_handsets mh
on ls.application_number = mh.r_order_application_number
where r_flow_type in  ('prime') and merchant_id_transaction = 3 and de_score_points between 0.01 and 0.084
    and location_id_transaction not in (11918,11966,104805,12596,10976,17240,103894,8507,149,17848,19019,41876,42099,43328,91964,92163,102961,104058)
    and (run_time_ms between 1563490800000 and 1570748400000) and first_inst_amount_60dpd > 0 and approval_amount >= 900
;