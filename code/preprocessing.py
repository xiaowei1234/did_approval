import numpy as np
import pandas as pd
import constants as c
from date_split_find import bucketize


def relevant_bucket_func(bucket, amount):
    return bucket in c.bucket_dict and amount in c.bucket_dict[bucket]


relevant_bucket = np.vectorize(relevant_bucket_func)


def pre_post(rtms):
    return 0 if rtms < c.time_split else 1


def filter_to_relevant(df):
    df['bucket'] = df.de_score_points.apply(bucketize)
    use = relevant_bucket(df.bucket, df.approval_amount)
#    df['time'] = df.rtms.apply(pre_post)
    return df.loc[use, ['post', 'de_score_points', 'treatment', 'fpd']]

#%%
if __name__ == '__main__':
    df = pd.read_csv(c.sql_data)
    clean_df = df.pipe(filter_to_relevant)
    clean_df.to_csv(c.preprocess_output, index=False)
