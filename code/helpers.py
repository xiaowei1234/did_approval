import pickle
import pandas as pd


def write_pickle(filepath, thing):
    """
    write out thing to filepath
    """
    if isinstance(thing, pd.core.frame.DataFrame):
        thing.to_pickle(filepath)
    else:
        with open(filepath, 'wb') as pfile:
            pickle.dump(thing, pfile)


def load_pickle(filepath, default=None):
    """
    loads pickled file specified by filepath or default if not found
    """
    if not os.path.isfile(filepath):
        return default
    if isinstance(default, pd.core.frame.DataFrame):
        return pd.read_pickle(filepath)
    with open(filepath, 'rb') as pfile:
        return pickle.load(pfile)