#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 11:16:38 2019

@author: xiaowei
"""

import os
import pandas as pd
import pymysql
import constants as c
#%%

def get_sql_conn():
    user = "risk"
    host = '127.0.0.1'
    port = 9999
    db = 'billfloat_production'
    password = os.environ['mysqlpw']
    return pymysql.connect(host=host, user=user, passwd=password, db=db, port=port)


def get_sql_str(name):
    with open(name) as f:
        sql_str = f.read()
    return sql_str


def get_df(sql_file, *args):
    sql_str = get_sql_str(sql_file).format(*args)
    conn = get_sql_conn()
    return pd.read_sql(sql_str, conn)  

#%%
if __name__ == '__main__':
    # ssh -L 9999:dbreplica.us-east-1.int.smartpaylease.com:3306 xwei@10.128.1.27
    fpd_df = get_df(c.sql_file)
    fpd_df.to_csv(c.sql_data, index=False)
    