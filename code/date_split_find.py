import pandas as pd
import constants as c
from sklearn.preprocessing import StandardScaler
from datetime import datetime
# import helpers



def bucketize(p):
    if p < 0.05:
        return '0.05'
    if p < .073:
        return '.073'
    return '0.084'


def find_date_of_shift(df):
    treat_df = df.loc[(df.treatment == 1) & (df.approval_amount >= 900), ['approval_amount', 'rtms', 'de_score_points']]
    treat_df['buckets'] = treat_df.de_score_points.apply(bucketize)
    treat_df['cnt'] = 1
    grouped_df = treat_df.groupby(['buckets', 'approval_amount']).agg({'cnt': 'sum', 'rtms': 'max'}).sort_index()
    grouped_df['dates'] = grouped_df.rtms.apply(lambda v: datetime.fromtimestamp(v).strftime('%Y-%m-%d %H:%M:%S'))
    last = grouped_df.loc[('0.84', 900), 'rtms'] + 1
    return last, grouped_df



if __name__ == '__main__':
    df = pd.read_csv(c.sql_data)
    last, date_split_df = find_date_of_shift(df)
    # helpers.write_pickle(c.data_path + 'treat_split.pkl', last)
    date_split_df.to_csv(c.data_path + 'date_split.csv')